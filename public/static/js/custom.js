$(document).ready(function() {
	$("#menutoggle .toggle").click(function() {
		$("#menutoggle .toggle").blur();
		if( $("#menutoggle").hasClass("open") ){
			$("#menu").transition({ x: "250px" });
			$("#menutoggle").removeClass("open").addClass("collapsed");
		}else{
			$("#menu").transition({ x: "-250px" });
			$("#menutoggle").removeClass("collapsed").addClass("open");
		}
	});
});
